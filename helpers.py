import os
import sys
import numpy as np
import lxml.etree as etree
from os.path import exists
import ogr
import gdal 
from osgeo.gdalconst import GA_ReadOnly
import shutil
import logging

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')

def get_mask_prob(s2_path):
    
    ns = {'xfdu': 'urn:ccsds:schema:xfdu:1',
          'safe': 'http://www.esa.int/safe/sentinel/1.1',
          'gml': 'http://www.opengis.net/gml'}
    
    path_manifest = os.path.join(s2_path, 'manifest.safe')
    
    root = etree.parse(path_manifest)
    

    sub_path = os.path.join(s2_path,
                            root.xpath('//dataObjectSection/dataObject/byteStream/fileLocation[contains(@href,("MSK_CLDPRB_20m.jp2"))]')[0].attrib['href'][2:])
    
    return sub_path

def get_band_path(s2_path, band):

    identifier = (s2_path.split('/')[-2] if s2_path.split('/')[-1] == '' else s2_path.split('/')[-1]).replace('.SAFE', '')

    
    latitude_band = identifier[41]
    grid_square  = identifier[42:44]

    
    
    ns = {'xfdu': 'urn:ccsds:schema:xfdu:1',
          'safe': 'http://www.esa.int/safe/sentinel/1.1',
          'gml': 'http://www.opengis.net/gml'}
    
    path_manifest = os.path.join(s2_path, 'manifest.safe')
    
    root = etree.parse(path_manifest)
    
    bands = [band]

    for index, band in enumerate(bands):

        sub_path = os.path.join(s2_path,
                                root.xpath('//dataObjectSection/dataObject/byteStream/fileLocation[contains(@href,("%s%s")) and contains(@href,("%s")) ]' % (latitude_band,
                                grid_square, 
                                band), 
                                  namespaces=ns)[0].attrib['href'][2:])
    
    return sub_path

   
    
def vegetation_index(vi_type, s2_product, output_name, scl_product=None):
    
    
    #"Spectral index","Equation landsat","Equation Sentinel–2","Citation"
    # "NDVI","(NIR–Red)/(NIR+Red)","(B8A–B4)/(B8A+B4)","*Rouse1973"
    # "NDBI","(MIR1–NIR)/(MIR1+NIR)","(B11–B8A)/(B11+B8A)","*Zha2003"
    # "MNDWI","(Green–NIR)/(Green+NIR)","(B3–B8A)/(B3+B8A)","Xu (2006)"
    # "NDMIR","(MIR1–MIR2)/(MIR1+MIR2)","(B11–B12)/(B11+B12)","*Lu2004"
    # "NDRB","(Red–Blue)/(Red+Blue)","(B4–B2)/(B4+B2)","Zhou et al. (2014)"
    # "NDGB","(Green–Blue)/(Green+Blue)","(B3–B2)/(B3+B2)","Zhou et al. (2014)"
    
    # ['B02', 'B03', 'B04', 'B8A', 'B11', 'B12']
    #   1      2      3      4      5      6
    gain = 10000
    
    scl = None
    
    if scl_product is not None:
        ds_scl = gdal.Open(scl_product)
        scl = ds_scl.GetRasterBand(1).ReadAsArray() 

        ds_scl = None
    
    ds = gdal.Open(s2_product)
    
    if vi_type == 'NDVI':
    
        b1 = ds.GetRasterBand(4).ReadAsArray() # B8A
        b2 = ds.GetRasterBand(3).ReadAsArray() # B04
    
    if vi_type == 'NDBI':
    
        b1 = ds.GetRasterBand(5).ReadAsArray() # B11
        b2 = ds.GetRasterBand(4).ReadAsArray() # B8A
        
    if vi_type == 'MNDWI':
    
        b1 = ds.GetRasterBand(2).ReadAsArray() # B03
        b2 = ds.GetRasterBand(4).ReadAsArray() # B8A
        
    if vi_type == 'NDMIR':
    
        b1 = ds.GetRasterBand(5).ReadAsArray() # B11
        b2 = ds.GetRasterBand(6).ReadAsArray() # B12
        
    if vi_type == 'NDRB':
    
        b1 = ds.GetRasterBand(3).ReadAsArray() # B04
        b2 = ds.GetRasterBand(1).ReadAsArray() # B02
        
    if vi_type == 'NDGB':
    
        b1 = ds.GetRasterBand(2).ReadAsArray() # B03
        b2 = ds.GetRasterBand(1).ReadAsArray() # B02
    
    width = ds.RasterXSize
    height = ds.RasterYSize
    
    input_geotransform = ds.GetGeoTransform()
    input_georef = ds.GetProjectionRef()
    
    ds = None
    
    upper = np.zeros((height, width), dtype=np.uint8)
    lower = np.zeros((height, width), dtype=np.uint8)
    veg_ind = np.zeros((height, width), dtype=np.uint8)
    
    #upper = b1[np.where((b1 >= (0.0 * gain)) & (b1 <= (1.0 * gain)))] - b2[np.where((b2 <= (1.0 * gain)) & (b2 <= (1.0 * gain)))]
    #lower = b1[np.where((b1 >= (0.0 * gain)) & (b1 <= (1.0 * gain)))] + b2[np.where((b2 <= (1.0 * gain)) & (b2 <= (1.0 * gain)))]
    upper = b1 - b2
    lower = b1 + b2
    
    veg_ind = upper / lower

    if scl is not None:
        veg_ind[np.where((scl == 0) | (scl == 1))] = -2

    driver = gdal.GetDriverByName('GTiff')
    
    output = driver.Create(output_name, 
                           width, 
                           height, 
                           1, 
                           gdal.GDT_Float32)
        
    output.SetGeoTransform(input_geotransform)
    output.SetProjection(input_georef)
    output.GetRasterBand(1).WriteArray(veg_ind)

    output.FlushCache()
   
    return True


    
def cog(input_tif):
    
    temp_tif = 'temp.tif'
    
    shutil.move(input_tif, temp_tif)
    
    translate_options = gdal.TranslateOptions(gdal.ParseCommandLine('-co TILED=YES ' \
                                                                    '-co COPY_SRC_OVERVIEWS=YES ' \
                                                                    ' -co COMPRESS=LZW'))

    ds = gdal.Open(temp_tif, gdal.OF_READONLY)

    gdal.SetConfigOption('COMPRESS_OVERVIEW', 'DEFLATE')
    ds.BuildOverviews('NEAREST', [2,4,8,16,32])
    
    ds = None

    ds = gdal.Open(temp_tif)
    gdal.Translate(input_tif,
                   ds, 
                   options=translate_options)
    ds = None

    os.remove('{}.ovr'.format(temp_tif))
    os.remove(temp_tif)

    
def cloud_mask(msk_cloud, prob, output_name):
    
    gdal.Translate('mask.tif', 
               msk_cloud,
               xRes=10, 
               yRes=10)

    ds = gdal.Open('mask.tif')

    mask = ds.GetRasterBand(1).ReadAsArray() 

    width = ds.RasterXSize
    height = ds.RasterYSize

    input_geotransform = ds.GetGeoTransform()
    input_georef = ds.GetProjectionRef()

    mask_threshold = np.zeros((height, width), dtype=np.uint8)

    mask_threshold[np.where(mask >= prob)] = 1

    driver = gdal.GetDriverByName('GTiff')

    output = driver.Create(output_name, 
                           width, 
                           height, 
                           1, 
                           gdal.GDT_Byte)

    output.SetGeoTransform(input_geotransform)
    output.SetProjection(input_georef)
    output.GetRasterBand(1).WriteArray(mask_threshold)

    output.FlushCache()
    
    os.remove('mask.tif')
    
    
